# This CI will only work for project members. CI for public contributors
# runs via a webhook on the merge requests. There's nothing you have to do if
# you want your changes tested -- created pipeline will be automatically
# linked in the merge request and appropriate labels will be added to it.
# Changes to this file will NOT be reflected in the webhook testing.

include:
  - project: cki-project/pipeline-definition
    ref: main
    file: kernel_templates.yml

stages:
  - test
  - build
  - deploy

workflow: !reference [.workflow]

.9-common:
  variables:
    builder_image_tag: latest
    srpm_make_target: dist-srpm
    native_tools: 'true'
    architectures: 'x86_64 ppc64le aarch64 s390x'
    POWER_BUILDER_SUFFIX: '-p9'

.trigger_c9s_pipeline:
  trigger:
    branch: c9s
  variables:
    name: centos-stream-9
    builder_image: registry.gitlab.com/cki-project/containers/builder-stream9
    kpet_tree_family: c9s
    kernel_type: 'upstream'  # Needs to be overriden due to inheriting from .rhel_common

.trigger_rhel9_pipeline:
  trigger:
    branch: rhel9
  variables:
    name: rhel9
    builder_image: quay.io/cki/builder-rhel9
    kpet_tree_family: rhel9

.realtime_check_c9s:
  trigger:
    branch: c9s-rt
  variables:
    merge_branch: main-rt
    name: kernel-rt-c9s

.realtime_check_rhel9:
  trigger:
    branch: rhel9-rt
  variables:
    merge_branch: main-rt
    name: kernel-rt-rhel9

.trigger_rhel9_build:
  extends: .trigger_rhel9_pipeline
  variables:
    skip_only_test: 'true'
    skip_results: 'true'

.realtime_full_c9s:
  variables:
    name: kernel-rt-c9s
    kpet_tree_family: c9s-rt
    RUN_ONLY_FOR_RT: 'true'

.realtime_full_rhel9:
  variables:
    name: kernel-rt-rhel9
    kpet_tree_family: rhel9-rt
    RUN_ONLY_FOR_RT: 'true'

# c9s CI
c9s_merge_request:
  extends: [.trusted, .merge_request, .rhel_common,
            .9-common, .trigger_c9s_pipeline]

c9s_realtime_check_merge_request:
  extends: [.trusted, .merge_request,
            .9-common,
            .realtime_check_common,
            .trigger_c9s_pipeline, .realtime_check_c9s]

c9s_baseline:
  extends: [.trusted, .baseline, .rhel_common,
            .9-common, .trigger_c9s_pipeline]

c9s_rhel9_compat_merge_request:
  extends: [.centos_stream_rhel_internal, .merge_request, .rhel_common, .with_notifications,
            .9-common, .trigger_rhel9_build]

c9s_rhel9_compat_baseline:
  extends: [.centos_stream_rhel_internal, .baseline, .rhel_common, .with_notifications,
            .9-common, .trigger_rhel9_build]

# c9s realtime branch CI
c9s_realtime_merge_request:
  extends: [.trusted, .merge_request,
            .9-common,
            .realtime_pipeline_common,
            .trigger_c9s_pipeline, .realtime_full_c9s]

c9s_realtime_baseline:
  extends: [.trusted, .baseline,
            .9-common,
            .realtime_pipeline_common,
            .trigger_c9s_pipeline, .realtime_full_c9s]

# RHEL9 CI
rhel9_merge_request:
  extends: [.internal, .merge_request, .rhel_common, .with_notifications,
            .9-common, .trigger_rhel9_pipeline]

rhel9_realtime_check_merge_request:
  extends: [.internal, .merge_request,
            .9-common,
            .realtime_check_common,
            .trigger_rhel9_pipeline, .realtime_check_rhel9]

rhel9_baseline:
  extends: [.internal, .baseline, .rhel_common, .with_notifications,
            .9-common, .trigger_rhel9_pipeline]

# RHEL9 realtime branch CI
rhel9_realtime_merge_request:
  extends: [.internal, .merge_request,
            .9-common,
            .realtime_pipeline_common,
            .trigger_rhel9_pipeline, .realtime_full_rhel9]

rhel9_realtime_baseline:
  extends: [.internal, .baseline,
            .9-common,
            .realtime_pipeline_common,
            .trigger_rhel9_pipeline, .realtime_full_rhel9]

# RHEL9 private CI (including RT branches)
rhel9_private_merge_request:
  extends: [.scratch, .merge_request, .rhel_common,
            .9-common, .trigger_rhel9_pipeline]

rhel9_private_realtime_check_merge_request:
  extends: [.scratch, .merge_request,
            .9-common,
            .realtime_check_common,
            .trigger_rhel9_pipeline, .realtime_check_rhel9]

rhel9_private_realtime_merge_request:
  extends: [.scratch, .merge_request,
            .9-common,
            .realtime_pipeline_common,
            .trigger_rhel9_pipeline, .realtime_full_rhel9]
